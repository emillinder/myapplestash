package com.millinder;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */
public class App {

    private static Random random = new Random(new Date().getTime());

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    private static final String[] colors = {"blue", "red", "yellow", "green", "black"};

    public static void main(String[] args) {

        Thread apples = new Thread(new Runnable() {
            private final org.slf4j.Logger logger = LoggerFactory.getLogger(App.class);

            @Override
            public void run() {
                boolean running = true;
                while (running) {
                    logger.info("{} apple(s) sold", random.nextInt(1000) + 1);
                    try {
                        Thread.sleep(2000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        apples.start();

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(() -> {
            int index = random.nextInt(colors.length);
            logger.info("apple color={}", colors[index]);
        }, 0, 3, TimeUnit.SECONDS);

    }

}
